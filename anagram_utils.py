#!/usr/bin/env python3

import os
from collections import defaultdict
from pathlib import Path


def load_dictionary(wordlength=None):
    """ Load the word list into a python list. Optional wordlength param
        restricts to words of that length.
    """
    dict_file = os.environ.get('DICT', None)
    if dict_file is None:
        dict_file = Path.home() / 'Jiko' / 'anagrams' / 'enable2.txt'

    with open(dict_file, 'r') as dictionary:

        if wordlength is not None:
            wordlist = [ e.strip().lower() for e in dictionary if len(e.strip()) == wordlength ]
        else:
            wordlist = [ e.strip().lower() for e in dictionary ]

    return wordlist


def sort_string(input_str):
    """ Create the normalized form of a word (letters in order) """
    return ''.join(sorted(input_str))


def words_from_string(input_str, norm_dict=None):
    ''' Return the possible single words by unscrambling input string '''
    if norm_dict is None:
        norm_dict = make_norm_dict(load_dictionary())
    return norm_dict.get(sort_string(input_str), [])


def make_norm_dict(wordlist):
    """ Create dictionary, keyed by normalized words (with letters in order),
    and values that are lists of words with those keys
    """
    words_by_norm_string = defaultdict(list)
    for word in wordlist:
        words_by_norm_string[sort_string(word)].append(word)
    return words_by_norm_string


if __name__ == '__main__':
    print("This is just a library.")
