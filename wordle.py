#!/usr/bin/env python

# TODO:
# - Fix rules for right char wrong position
# - Improve QWERTY keyboard showing wrong and unguessed letters
# - Create simplified (more mainstream) dictionary

'''
Basic implementation of Wordle
'''

import sys
import random

import anagram_utils as au


# Global
word_length = 5

def disp_char(c, stat):
    ''' disp_char -- return the input char with ASCII control codes to colorize
    '''
    char_cols = {
        'wrong': ('bg_gray'),
        'riterite': ('bg_green'),
        'riterong': ('bg_yellow'),
    }

    return color_char(c, char_cols[stat])


def color_char(c, col, bold=False):
    esc = u"\u001b["
    reset = u"\u001b[0m"
    palette = {
        'gray':  '30;1m',
        'yellow': '33m',
        'green': '32m',
        'red': '31m',
        'black': '30m',
        'white': '37m',

        'bg_gray':  '40;1m',
        'bg_yellow': '43;1m',
        'bg_green': '42;1m',
        'bg_red': '41;1m',
        'bg_white': '47;1m',
    }
    return esc + palette[col] + c + reset


def colorize_word(guess, theword):
    correct_count = 0
    disp_string = ''
    for i, L in enumerate(guess):
        if L in theword:
            if guess[i] == theword[i]:
                disp_string += disp_char(L, 'riterite')
                correct_count += 1
            else:
                disp_string += disp_char(L, 'riterong')
        else:
            disp_string += disp_char(L, 'wrong')
    return (disp_string, correct_count)


def show_stack(all_guessed_words, theword):
    correct_count = 0
    for w in all_guessed_words:
        disp_string, correct_count = colorize_word(w, theword)
        print("    ", disp_string)
    return correct_count


def show_keyboard(all_guesses, bad_guesses):
    #print("DEBUG: all_guesses: ", all_guesses)
    #print("DEBUG: bad_guesses: ", bad_guesses)
    qwerty = '''
    Q W E R T Y U I O P
     A S D F G H J K L
      Z X C V B N M'''

    for L in qwerty:
        if L not in all_guesses and L != ' ' and L != "\n":
            qwerty = qwerty.replace(L, color_char(L, 'bg_yellow'))

    for L in all_guesses:
        if L in bad_guesses:
            qwerty = qwerty.replace(L, color_char(L, 'bg_red'))
        else:
            qwerty = qwerty.replace(L, color_char(L, 'bg_green'))

    print(qwerty)
    print("\n")


def repl(theword):
    try:
        all_guessed_words = []
        bad_guesses = set()
        all_guesses = set()
        while True:
            try:
                correct_count = show_stack(all_guessed_words, theword)
                if correct_count == word_length:
                    return(len(all_guessed_words))

                show_keyboard(all_guesses, bad_guesses)

                _in = input("Guess a word ({} letters):  ".format(word_length))
                _in = _in.upper()
                all_guessed_words.append(_in)

                [all_guesses.add(e) for e in _in]
                [bad_guesses.add(e) for e in _in if e not in theword]

            except KeyboardInterrupt as e:
                print("\nExiting")
                sys.exit(1)
            except EOFError as e:
                print("\nThe word was:  {}\nExiting".format(theword))
                sys.exit(1)
    except KeyboardInterrupt as e:
        print("\nExiting")
        sys.exit(1)


if __name__ == "__main__":

    words = au.load_dictionary(wordlength=word_length)
    theword = random.choice(words)
    theword = theword.upper()
    trycount = repl(theword)
    print("Congratulations!  You got the word in {} tries!".format(trycount))
