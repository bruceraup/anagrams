#!/usr/bin/env python3

import sys

import anagram_utils as au


def get_palindromes():

    print("Palindromic words in word list:")
    word_list = au.load_dictionary()

    pals = []

    for key in word_list:
        if key[::-1] == key:
            pals.append(key.strip())

    return pals


def print_pal_list(pals, sort_method='alpha'):
    if sort_method == 'length':
        sorted_pals = sorted(pals, key=lambda e: len(e))
    elif sort_method == 'alpha':
        sorted_pals = sorted(pals)
    else:
        print("Bad sort method:  ", sort_method)
        return
    for w in sorted_pals:
        print(w)


if __name__ == '__main__':
    if len(sys.argv) > 1:
        if sys.argv[1] == '-h':
            print(f"Usage:  {sys.argv[0]} [-L]")
            print("where -L means to sort by word length (otherwise sort is alphabetical)")
            sys.exit(0)
        elif sys.argv[1] == '-L':
            sort_method = 'length'
    else:
        sort_method = 'alpha'

    print_pal_list(get_palindromes(), sort_method=sort_method)
