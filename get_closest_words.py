#!/usr/bin/env python

''' Get the closest N words to the input words, according to the Levenshtein
distance. '''

import sys

import editdistance as ed
import anagram_utils as au

def closest_words(word, D, N=1):
    ''' closest_words -- return the closest N words according to the
    Levenshtein distance

    inputs:

        word        input word that one wants the closest words to

        D           dictionary of possible words
    '''

    if N < 1:
        raise RuntimeError("closest_words: N must be >= 1")

    distances = []
    for w in D:
        dist = ed.levenshteinDistance(word, w)
        if len(distances) >= N:
            distances = sorted(distances + [(w, dist)], key=lambda x: x[1])[0:N]
        else:
            distances.append((w, dist))

    return distances


if __name__ == '__main__':
    inwords = sys.argv[1:]

    D = au.load_dictionary()
    names = ['frank', 'fred', 'tony', 'sara', 'laura']

    for w in inwords:
        print(f"{w:20s}: {closest_words(w, D, N=4)}")
        #print(f"{w:20s}: {closest_words(w, names, N=4)}")


