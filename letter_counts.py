#!/usr/bin/env python3
""" Print counts of letters in the master word list """

from collections import Counter

import anagram_utils as au

d = au.load_dictionary()
letter_counts = Counter()
for word in d:
    letter_counts.update(word)

print("Letter frequencies in word list:")
for k in sorted(letter_counts.keys(), key=lambda e: letter_counts[e], reverse=True):
    print("{}:  {}".format(k, letter_counts[k]))
