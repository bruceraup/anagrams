#!/usr/bin/env python3

# See, for example, http://www.macnamarasband.com/split/split.html

'''
_ i n _ _
  c r 

leads to

sinew
screw

To solve this with this program, enter

.in.. .cr..

as the patterns.

'''

import sys
import re

import anagram_utils as au


def all_in(letters, word):
    for L in word:
        if L not in letters:
            return False
    return True


def switch_letters(word, pat2):
    pat2_letters = pat2.strip('.')
    idx = pat2.find(pat2_letters)
    w_list = list(word)
    w_list[idx:idx+2] = list(pat2_letters)
    return ''.join(w_list)


def split_matches(d, pat1, pat2):
    p1 = re.compile('^' + pat1 + '$')
    first_matches = []
    for w in d:
        m = p1.match(w)
        if m:
            first_matches.append(m.group(0))

    # Switch to other letter pair and see what is still a word
    first_list_translated = [switch_letters(w, pat2) for w in first_matches]
    for w1, w2 in zip(first_matches, first_list_translated):
        if w2 in d:
            print("{}  :::  {}".format(w1, w2))


def exit_usage():
    print("Usage:  {} <pattern1> <pattern2>".format(sys.argv[0]))
    print("\nExample: {} .ro. .uf.".format(sys.argv[0]))
    sys.exit(1)


def main():
    d = au.load_dictionary()
    if len(sys.argv) != 3:
        exit_usage()

    pat1, pat2 = sys.argv[1:3]

    if len(pat1) != len(pat2):
        exit_usage()

    split_matches(d, pat1, pat2)


if __name__ == '__main__':
    main()
