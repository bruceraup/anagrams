#!/usr/bin/env python3

from itertools import combinations

def rem(L, subset):
    """ Input list, and subset list; return big list with subset removed
    (remainder)
    """
    M = [e for e in L]
    for e in subset:
        M.remove(e)
    return M

L = [1,2,3,4,5]
print("Starting list:  ", L)
for c in combinations(L, 3):
    print(c, "Remainder: ", rem(L, c))

L = [1,2,2,3,3]
print("\nStarting list:  ", L)
for c in combinations(L, 3):
    print(c, "Remainder: ", rem(L, c))

