#!/home/braup/anaconda3/bin/python3 -u

import sys
from itertools import combinations
from collections import defaultdict

import anagram_utils as au
import spelling_bee as sb

def find_max_spelling_bee_score():
    max_score = 0
    max_letters = ''
    max_middle = ''
    alphabet = 'abcdefghijklmnopqrstuvwxyz'
    d = au.load_dictionary()

    num_games = 7 * len(list(combinations(alphabet, 7)))
    chunk_size = int(num_games / 10000.0)
    game_count = 0

    score_counts = defaultdict(int)

    for letters in combinations(alphabet, 7):
        letters = ''.join(letters)
        for middle in letters:

            # Give status output to stderr
            if game_count % chunk_size == 0:
                sys.stderr.write("{i} of {N} ({p}%)\n".format(
                    i=game_count, N=num_games, p=100*game_count/num_games))

            game_count += 1

            results, score = sb.gen_bee(d, letters, middle)

            score_counts[score] += 1

            if score > max_score:
                max_score = score
                max_letters = letters
                max_middle = middle
                sys.stderr.write("New high score: {} ({}, {})\n".format(max_score, letters, middle))

    return(max_score, max_letters, max_middle, score_counts)

if __name__ == '__main__':

    (max_score, max_letters, max_middle, score_counts) = find_max_spelling_bee_score()
    for score in sorted(score_counts.keys()):
        print(score, score_counts[score])
    sys.stderr.write("With letters {} and {} as middle letter, score is {}\n".format(max_letters, max_middle, max_score))
