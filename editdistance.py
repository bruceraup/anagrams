#!/usr/bin/env python3

def levenshteinDistance(s1, s2):
    if len(s1) > len(s2):
        s1, s2 = s2, s1

    distances = range(len(s1) + 1)
    #print("Before loops, distances = ", distances)
    for i2, c2 in enumerate(s2):
        distances_ = [i2+1]
        for i1, c1 in enumerate(s1):
            if c1 == c2:
                distances_.append(distances[i1])
            else:
                distances_.append(1 + min((distances[i1], distances[i1 + 1], distances_[-1])))
        distances = distances_
        #print("        Mid loops, distances = ", distances)
    return distances[-1]


if __name__ == '__main__':

    word_pairs = [
        ('the', 'this'),
        ('weather', 'whether'),
        ('this', 'that'),
        ('biffy', 'giffy'),
        ('lone', 'line'),
        ('lone', 'lone'),
        ('abcd', 'xhzy'),
        ('a', 'ab'),
    ]

    for w1, w2 in word_pairs:
        print("{:>10} -> {:<10}:  {}".format(w1, w2, levenshteinDistance(w1, w2)))
