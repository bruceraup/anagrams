#!/usr/bin/env python3

import sys
import os.path

import anagram_utils as au


def main():
    if len(sys.argv) < 2:
        print("Usage:  {} num_letters".format(os.path.basename(__file__)))
        sys.exit(1)

    num_letters = int(sys.argv[1])
    print("Anagrams of length", num_letters, ":")
    word_dict = au.make_norm_dict(au.load_dictionary())

    for key in word_dict:
        if len(key) == num_letters and len(word_dict[key]) > 1:
            print("  ".join(word_dict[key]))


if __name__ == '__main__':
    main()
