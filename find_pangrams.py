#!/usr/bin/env python3
""" Print words with 7 unique letters """

import anagram_utils as au

d = au.load_dictionary()

for word in d:
    letters = set(word.lower())
    if len(letters) == 7 and 's' not in letters:
        print(word)
