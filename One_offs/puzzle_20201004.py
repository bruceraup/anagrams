#!/usr/bin/env python3

'''
Find an 8-letter word with three syllables, each of which contains the letter
"i", but where none of them have the "i" sound, long or short.

The answer seems to be "daiquiri".
'''

import anagram_utils as au

wordlist = au.load_dictionary()

for word in wordlist:
    if len(word) != 8:
        continue
    if list(word).count('i') != 3:
        continue
    print(word)
