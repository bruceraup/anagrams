#!/usr/bin/env python3

import sys

import anagram_utils as au


def fetch_input_strings():
    if len(sys.argv) < 2:
        print("Usage:  {} string [string ...]".format(sys.argv[0]))
        sys.exit(1)

    return sys.argv[1:]


def main():
    input_strings = fetch_input_strings()

    known_strings = au.load_dictionary()
    words_by_norm_string = au.make_norm_dict(known_strings)

    for input_string in input_strings:
        sorted_string = au.sort_string(input_string)
        words = au.words_from_string(sorted_string, words_by_norm_string)
        if len(words) > 0:
            print("Possible Words for Scrambled Word %s = %s" % (input_string, words))
        else:
            print("No matches found for word %s" % (input_string))


if __name__ == '__main__':
    main()
