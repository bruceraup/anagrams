# Word programs #

Code for doing various things with words, in including anagrams.  The code is
vanilla Python that does not depend on any external libraries.

## Included programs ##

* `all_anagrams.py`

    Print all anagrams (one or two words, currently) in letters given on command line

* `editdistance.py`

    Test of the Levenshtein Distance algorithm

* `find_anagram_sets.py`

    Print pairs of words that are anagrams of each other, given a length of word

* `find_palindromes.py`

    Print all palindromes in the word list

* `letter_counts.py`

    Print the number of occurrences of each letter in the word list

* `spelling_bee.py`

    Print all words in a "spelling bee" puzzle and the total score

* `split_decisions.py`

    Find solutions to "split decisions" puzzles

* `unscramble_words.py`

    Attempts to unscramble given letters and prints results

## Other files ##

There are some automated tests (using nosetests) in `test_anagram_utils.py`.

The file `anagram_utils.py` contains utility routines.

# Contact #

* Bruce Raup (bruceraup@gmail.com)
