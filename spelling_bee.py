#!/usr/bin/env python3

import sys
import argparse
import string

import anagram_utils as au


def setup_argument_parser():
    """setup_argument_parser -- Set up command line options.

    -h or --help for help is automatic

    Args:
        None

    Returns:
        argparse ArgumentParser object

    """
    s_help = '''Scoring method.  "4" is similar to the online NYT spelling bee,
                where 4-letter words are allowed, longer words are scored
                higher.  "5" is scored like the NYT magazine, where words must
                be 5 letters or longer, and only pangrams score higher (3
                points rather than 1).'''

    p = argparse.ArgumentParser()
    p.add_argument('-L', '--letters', type=letters_type, help='The 7 input letters, as LLLLLLL,M')
    p.add_argument('-s', '--scoring', default='5', choices=['4', '5'], help=s_help)
    return(p)


def letters_type(letters):
    letters = letters.lower()
    all_lets, mid = parse_letters_arg(letters)
    if len(all_lets) == 7 and all([e.lower() in string.ascii_lowercase for e in all_lets]) \
            and len(mid) == 1 and mid in all_lets:
        return letters
    else:
        raise ValueError('letters must be in the format LLLLLLL,M')


def parse_letters_arg(letters_arg):
    all_lets, mid = letters_arg.split(',')
    all_lets = list(set(all_lets))
    return (all_lets, mid)


def all_in(letters, word):
    for L in word:
        if L not in letters:
            return False
    return True


def all_used(letters, word):
    for L in letters:
        if L not in word:
            return False
    return True


def get_score(word, score_method='5', pangram=False):
    if score_method == '5':
        if pangram:
            return 3
        else:
            return 1

    elif score_method == '4':
        if len(word) == 4:
            score = 1
        else:
            score = len(word)

        if pangram:
            score += 7
        return score
    else:
        return 0


def gen_bee(d, letters, middle, score_method='5'):
    min_length = int(score_method)
    score = 0
    results = []
    for word in d:
        if len(word) >= min_length and middle in word and all_in(letters, word):
            if all_used(letters, word):
                word_score = get_score(word, score_method, True)
                if score_method == '5':
                    results.append(word + ' 3')
                else:
                    results.append(word + f' (*** {word_score})')
            else:
                word_score = get_score(word, score_method, False)
                if score_method == '4':
                    results.append(word + f' ({word_score})')
                else:
                    results.append(word)

            score += word_score

    return(results, score)


def main():
    p = setup_argument_parser()
    args = p.parse_args()

    letters, middle = parse_letters_arg(args.letters)

    d = au.load_dictionary()
    score_method = args.scoring

    results, score = gen_bee(d, letters, middle, score_method)
    print("\n".join(results))
    print("\nNumber of words:  {}  Score:  {}".format(len(results), score))


if __name__ == '__main__':
    main()
