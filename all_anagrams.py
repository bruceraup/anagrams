#!/usr/bin/env python3

'''
TODO:

    - parallelize

    Can also find combinations and remainders using the method described at
    https://stackoverflow.com/questions/39545543/python-combinations
'''

import sys
from itertools import combinations
from itertools import product

import anagram_utils as au


def fetch_input_strings():
    if len(sys.argv) < 2:
        print("Usage:  {} string [string ...]".format(sys.argv[0]))
        sys.exit(1)
    return ''.join(sys.argv[1:]).lower()


def remainder(L, subset):
    """ Input list or string, and subset list or string; return big list with
    subset removed (remainder)
    """

    # Make list copy of input list or string
    M = [e for e in L]
    for e in subset:
        M.remove(e)
    return "".join(M)


def anagrams_one_set(subset, comp_set, wordlist):
    '''
    anagrams_one_set -- return a list of anagrams from one subset of the input and its complementary set

    Inputs:

        subset      subset of the input string
        comp_set    complement of subset (remainder)
        wordlist    the dictionary, keyed by normalized words; values are lists of words

    Returns:

        A list of tuples of sorted words.  Each tuple represents a complete
        anagram of the input string (subset + comp_set).

    '''

    combo_anagrams = wordlist[subset]
    rem_anagrams   = wordlist[comp_set]

    if len(subset) > 0 and len(comp_set) > 0:
        all_anagrams = list(product(combo_anagrams, rem_anagrams))
        all_anagrams_sorted = list([tuple(sorted(e)) for e in all_anagrams])
        return all_anagrams_sorted
    elif len(subset) > 0:
        rtn = [(e,) for e in combo_anagrams]
        return rtn
    elif len(comp_set) > 0:
        rtn = [(e,) for e in rem_anagrams]
        return rtn
    else:
        return [()]


def combinations_remainders(instring, N):

    for combo in combinations(instring, N):
        combo_sort = au.sort_string("".join(combo))
        rem = au.sort_string(remainder(instring, combo_sort))
        yield(combo_sort, rem)


def product_tuples(LoT1, LoT2):
    ''' product_tuples -- like itertools.product, but for lists of tuples

        Example:    LoT1 = [('red',)]
                    LoT2 = [('ab', 'a'), ('ba', 'a')]

                    Output should be:

                        [('red', 'ab', 'a'), ('red', 'ba', 'a')]

        itertools.product would give:

                        [(('red',), ('ab', 'a')), (('red',), ('ba', 'a'))]
    '''
    prod = product(LoT1, LoT2)
    prod_flat = list([sum(e, ()) for e in prod])
    return prod_flat

# Global that all levels of recursion can see
string_seen = {}

def find_all_anagrams(instring, wordlist):

    all_anagrams = []

    string_seen[instring] = 1

    min_size = int(len(instring) / 2.0 + 0.5) - 1

    start_sizes = range(len(instring), min_size-1, -1)  # [N, N-1, ..., min_size]
    for N in start_sizes:
        for subset, comp_set in combinations_remainders(instring, N):
            anagrams = anagrams_one_set(subset, comp_set, wordlist)
            for a in anagrams:
                all_anagrams.append(a)

            if len(comp_set) > 1 and comp_set not in string_seen:
                more_anagrams = find_all_anagrams(comp_set, wordlist)
                wl = [(e,) for e in wordlist[subset]]
                prod = product_tuples(wl, list(more_anagrams))
                prod_sorted = sorted(list(prod))
                for p in prod_sorted:
                    all_anagrams.append(p)

    return all_anagrams


def main():
    input_string = fetch_input_strings()

    # Normalized word dictionary
    wordlist = au.make_norm_dict(au.load_dictionary())

    all_anagrams = set()

    anagrams = find_all_anagrams(input_string, wordlist)
    for a in anagrams:
        all_anagrams.add(tuple(sorted(list(a))))

    try:
        for a in sorted(list(all_anagrams), key=lambda x: (len(x), x)):
            print(" ".join(a))
    except (BrokenPipeError):
        print("Pipe closed before full list was printed.", file=sys.stderr)


if __name__ == '__main__':
    main()
