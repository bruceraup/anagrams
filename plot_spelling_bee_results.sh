#!/bin/bash

infile='max_spelling_bee_results.txt'

gmt psxy $infile -R0/1300/0/100000 -Bf20a200:Score:/f10000a10000:'Number of games':WS -JX12c -P > spellbee_plot.ps

gmt psconvert -A spellbee_plot.ps
