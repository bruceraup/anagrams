#!/usr/bin/env python

import sys
import argparse
import re

import anagram_utils as au


def setup_argument_parser():
    """setup_argument_parser -- Set up command line options.

    -h or --help for help is automatic

    Args:
        None

    Returns:
        argparse ArgumentParser object

    """

    p = argparse.ArgumentParser()
    p.add_argument('patts', metavar='patts', type=str, nargs='+',
            help='<current_pattern> <good_letters> <bad_letters> Example:  .e... dew bast')
    return(p)


def pattern_matches(patt, dictword):
    if re.match(patt, dictword):
        return True
    else:
        return False


def all_in_word(good_letters, dictword):
    return set(good_letters).issubset(set(dictword))


def none_in_word(bad_letters, dictword):
    return set(bad_letters).isdisjoint(set(dictword))


def main():

    p = setup_argument_parser()
    args = p.parse_args()

    patt, good_letters, bad_letters = args.patts[0], args.patts[1], args.patts[2]

    d = au.load_dictionary()

    for w in d:
        if len(w) == 5 and \
           pattern_matches(patt, w) and \
           all_in_word(good_letters, w) and \
           none_in_word(bad_letters, w):
            print(w)


if __name__ == '__main__':
    main()
