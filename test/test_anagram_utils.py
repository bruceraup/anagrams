import anagram_utils as au


def test_dict_entries():
    wd = au.make_norm_dict(au.load_dictionary())
    expected = ['math']
    assert wd['ahmt'] == expected


def test_sort_string():
    s = 'cbdea'
    expected = 'abcde'
    assert(au.sort_string(s) == expected)


def test_make_norm_dict():
    input_wordlist = ['list', 'lits', 'silt', 'slit', 'tils']
    expected = {'ilst': input_wordlist}
    normdict = au.make_norm_dict(input_wordlist)
    assert normdict == expected
