#!/bin/bash

tr -cs A-Za-z '' < $1 | tr A-Z a-z | sort | uniq -c | sort -rn | head
