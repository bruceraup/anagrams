#!/usr/bin/env python3

import os
import argparse
import random

import anagram_utils as au


class Hangman(object):
    def __init__(self, d, min_length=5):
        # Current game state
        self.the_word = []
        self.guesses = []
        self.mask = []
        self.num_wrong = 0
        # Per-game config
        self.min_length = min_length
        self.wordlist = d
        self.max_wrong = 7
        # Multi-game stats
        self.word_no = 0
        self.total_guesses = 0
        self.total_wrong = 0

    def _clear_screen(self):
        os.system('cls' if os.name == 'nt' else 'clear')

    def play_game(self):
        self.new_word()
        self.print_game()
        while True:
            guess = input("Guess: ").lower()
            if len(guess) != 1:
                self.print_game("Enter just one letter!")
                continue

            if guess in self.guesses:
                self.print_game(f"Already guessed {guess}.")
                continue

            self.guesses.append(guess)
            self.total_guesses += 1
            self.guesses = sorted(self.guesses)

            if guess not in self.the_word:
                self.num_wrong += 1
                self.total_wrong += 1
                if self.num_wrong >= self.max_wrong:
                    self.print_game(f"The word was: {''.join(self.the_word)}!")
                    break
            else:
                # Replace hyphens with letters in mask
                for i in range(len(self.the_word)):
                    if self.the_word[i] == guess:
                        self.mask[i] = self.the_word[i]

            if '-' not in self.mask:
                self.print_game("You got it!")
                break
            else:
                self.print_game()


    def new_word(self):
        '''
        select a word at random from the dictionary
        '''
        while True:
            word = random.choice(self.wordlist)
            if len(word) >= self.min_length:
                self.the_word = list(word)
                break

        self.word_no += 1
        self.guesses = []
        self.num_wrong = 0
        self.mask = ['-'] * len(self.the_word)


    def print_game(self, message=""):
        self._clear_screen()

        gameboard = f'''
    {self.make_scaffold()}

     Word:  {''.join(self.mask)}

    Guessed: {''.join(self.guesses)}

    Word #:                {self.word_no}
    Misses on this word:   {self.num_wrong}
    Average misses/word:   {self.total_wrong / self.word_no}
    Average guesses/word:  {self.total_guesses / self.word_no}

    {message}
    '''
        print(gameboard)


    def make_scaffold(self):
        scaf = [
                '''
         ______
         |    |
         |
         |
         |
         |
       __|_____
       |      |___
       |_________|
       ''',
                '''
         ______
         |    |
         |    O
         |
         |
         |
       __|_____
       |      |___
       |_________|
       ''',
                '''
         ______
         |    |
         |    O
         |    |
         |
         |
       __|_____
       |      |___
       |_________|
       ''',
                '''
         ______
         |    |
         |    O
         |    |
         |    |
         |
       __|_____
       |      |___
       |_________|
       ''',
                '''
         ______
         |    |
         |    O
         |    |
         |    |
         |   /
       __|_____
       |      |___
       |_________|
       ''',
                '''
         ______
         |    |
         |    O
         |   /|
         |    |
         |   /
       __|_____
       |      |___
       |_________|
       ''',
                '''
         ______
         |    |
         |    O
         |   /|\\
         |    |
         |   /
       __|_____
       |      |___
       |_________|
       ''',
                '''
         ______
         |    |
         |    O
         |   /|\\
         |    |
         |   / \\
       __|_____
       |      |___
       |_________|
       ''',
        ]
        return scaf[self.num_wrong]


def setup_argument_parser():
    """setup_argument_parser -- Set up command line options.

    -h or --help for help is automatic

    Args:
        None

    Returns:
        argparse ArgumentParser object

    """

    p = argparse.ArgumentParser()
    p.add_argument('-L', '--length_min', type=int, default=5, help='Minimum length of word')
    return(p)



def main():
    p = setup_argument_parser()
    args = p.parse_args()

    d = au.load_dictionary()
    H = Hangman(d, args.length_min)

    while True:
        H.play_game()
        ans = input("Another game? (Y/n) ") or 'y'
        if not ans.lower().startswith('y'):
            break


if __name__ == '__main__':
    main()

