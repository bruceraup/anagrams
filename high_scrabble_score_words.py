#!/usr/bin/env python3
'''
Find the top N high-scoring Scrabble words.
'''


import sys

import anagram_utils as au


def scrabble_score_for(word):
    '''
    Return Scrabble score for input word.  No score modifiers are applied.
    '''
    letter_scores = {
        'a':    1,
        'b':    3,
        'c':    3,
        'd':    2,
        'e':    1,
        'f':    4,
        'g':    2,
        'h':    4,
        'i':    1,
        'j':    8,
        'k':    5,
        'l':    1,
        'm':    3,
        'n':    1,
        'o':    1,
        'p':    3,
        'q':    10,
        'r':    1,
        's':    1,
        't':    1,
        'u':    1,
        'v':    4,
        'w':    4,
        'x':    8,
        'y':    4,
        'z':    10,
    }
    score = sum([letter_scores[e.lower()] for e in word])
    return score


def main():
    if len(sys.argv) > 1:
        words = sys.argv[1:]
    else:
        words = au.load_dictionary()

    all_scores = []
    for w in words:
        all_scores.append((w, scrabble_score_for(w)))

    print("Scores:")
    try:
        for w in sorted(all_scores, key=lambda e: e[1], reverse=True):
            print("  {:30s}  {}".format(w[0], w[1]))
    except (BrokenPipeError):
        print("(Pipe closed before full list printed)", file=sys.stderr)


if __name__ == '__main__':
    main()
